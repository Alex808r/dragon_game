# frozen_string_literal: true

require_relative 'character'

class Hero < Character
  attr_reader :health_points

  MIN_DMG = 1
  MAX_DMG = 7
  POTION_STRENGTH = 10

  def initialize
    @hit_points = 20
    @health_points = 2
    @drunk_potion = false
  end

  def min_dmg
    @drunk_potion ? 0 : self.class::MIN_DMG
  end

  def drink_potion
    if @health_points.positive?
      @hit_points += POTION_STRENGTH
      @health_points -= 1
    end
    @drunk_potion = true
  end

  private

  def after_attack
    @drunk_potion = false if @drunk_potion
  end
end
