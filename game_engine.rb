# frozen_string_literal: true

require_relative 'hero'
require_relative 'dragon'

class GameEmgine
  def initialize
    @hero = Hero.new
    @dragon = Dragon.new
    @hero_turn = true
  end

  def run
    loop do
      puts "Здоровье героя #{@hero.hit_points}"
      puts "Здоровье дракона #{@dragon.hit_points}"

      @hero_turn ? do_hero_turn : do_dragon_turn

      break if check_win?

      @hero_turn = !@hero_turn
    end
  end

  def check_win?
    hero_win?
    dragon_win?
  end

  def hero_win?
    if @dragon.hit_points <= 0
      puts 'Герой победил!'
      true
    end
  end

  def dragon_win?
    if @hero.hit_points <= 0
      puts 'Дракон победил!'
      true
    end
  end

  def do_hero_turn
    puts "\nХодит герой!"
    # Ход героя
    if hero_choice == 'Z'
      @hero.drink_potion
    else
      dmg = @hero.attack @dragon
      puts "Герой ударил на #{dmg}"
    end
  end

  def hero_choice
    loop do
      puts "Что делаем? Введите Z для зелья (#{@hero.health_points}) или А для атаки:"

      user_answer_char = gets.strip[0].upcase

      return user_answer_char if %w[A Z].include?(user_answer_char)

      puts 'Ответ от А или Z!'
    end
  end

  def do_dragon_turn
    puts "\nХодит Дракон!"
    # Ход дракона
    dmg = @dragon.attack(@hero)
    puts "Дракон ударил на #{dmg}"
  end
end
